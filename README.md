# Ejemplo Framework Flask + sqlalchemy

Ejemplo del Framework para la clase de Patrones de diseño de software todo el entorno esta montado en ambiente windows 7 o superior 

## Prerequisitos

Debe realizar la instalación de cada una de las aplicaciones para poder completar el ejemplo de manera correcta.

* [Git](https://git-scm.com/)
* [python](https://www.python.org/) (con pip)
    * `Tener en cuenta seleccionar y marcar la casilla como se muestra en la imagen`
    ![alt text](https://i.ibb.co/Sm5mGhw/2019-04-02.png)
   
* [virtualenv](https://docs.python-guide.org/dev/virtualenvs/#lower-level-virtualenv)
    * `Solo realizar los pasos de instalación con pip`
        * `pip install virtualenv`
* [postman](https://www.getpostman.com/downloads/)

## Instalación

Abrir la consola ingresar a la ruta donde desea clonar el Repositorio
* `git clone https://bitbucket.org/luysk891/flask_and_sqlalchemy` este repositorio
* Crear el entorno virtual
    * `virtualenv <nombre_entorno>`
    * `Ingresar a la carpeta  \flask_and_sqlalchemy\<nombre_entorno>\Scripts`
    * Una vez dentro de la carpeta activar el entorno virutal con 'activate'
        * `Instalacion dependencias`
            * `pip install flask`
            * `pip install flask_sqlalchemy`
            * `pip install flask_marshmallow`
            * `pip install marshmallow-sqlalchemy`
* `Al completar la activacion del entorno e instalacion de las dependencias requeridas  ingresamos a la ruta del ejemplo 1) \flask_and_sqlalchemy\primer ej`

## Ejecutando

* `python app.py y presionamos la tecla intro`
* Ingresa a tu API en [http://127.0.0.1:5000](http://127.0.0.1:5000).
